
This is a simple module that allows a site admin to change the page text
that a user sees after following the reset password one time email link.
Options allow for a few html tags, tokens, and site Representative picture which
may also be added by custom token created by this module. Site Representative is
chosen by autocompletion field that populates site Users.

Future plans include options for modifying other admin forms that currently
lack gui interface.


Installation
------------
1) Copy the user_forms_help directory to the modules
folder in your installation.

2) Enable the module using Administer -> Modules (/admin/modules)


Configuration
-------------
The configuration for user_forms_help is found via:
admin/config/user-forms-help
